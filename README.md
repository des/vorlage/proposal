# Final Thesis Proposal Template

Welcome to the final thesis proposal template for the [Group for Digitalized Energy Systems](https://uol.de/des) at the Carl von Ossietzky University Oldenburg.

## Instructions:
* Please replace placeholders with your name and the title of your thesis.
* You can switch between languages for all commands by changing the language order in the `\usepackage` command, e.g., `\usepackage[english, ngerman]{babel}`.
* All categories provided in the template are suggestions; feel free to structure your proposal differently if needed.

## File Structure:
The template consists of multiple files:

* **`proposal.tex`:** The main document structure, which includes placeholders for the title, author, and various content sections.
* **`setup.tex`:** Configures necessary packages and settings, incorporated into the main document via `\input{setup.tex}`.
* **`references.bib`:** Contains the bibliography entries for managing citations, referenced in the main document through the `biblatex` package.

## Document Content Guidelines:
The main document provides suggestions on structure and content length:

* Sections like "Introduction" are language-dependent and may vary in content.
* Overall length should not exceed five pages, excluding the preliminary outline, schedule, and bibliography.
* The inclusion of additional sections is at the author's discretion.

## Compatibility
This template has been tested with various LaTeX editors and compilers, including Overleaf, MiKTeX, TeXworks, and the pdflatex backend.

## License
This template is licensed under the [Creative Commons Attribution 4.0 International License (CC-BY 4.0)](https://creativecommons.org/licenses/by/4.0/legalcode) by the Group for Digitalized Energy Systems (UOL-DES).
